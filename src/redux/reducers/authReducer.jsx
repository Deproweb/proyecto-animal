import { REGISTER, LOGIN, LOGOUT } from "../actions/authAction";

const INITTIAL_STATE = {
    user: null,
    error: null,
}

export const authReducer = (state = INITTIAL_STATE, action) => {

    switch (action.type) {

        case REGISTER:
            return {user: action.payload}

        case LOGIN:
            return {user: action.payload}

        case LOGOUT:
            return {}
            
        default:
            return state
    }
};
