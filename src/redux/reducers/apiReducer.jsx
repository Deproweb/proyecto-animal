import { GET_POST } from "../actions/apiAction"

const INITIAL_STATE = {

    animals: []

}

export const apiReducer = (state = INITIAL_STATE, action) => {

    switch (action.type) {

        case GET_POST:
            return {
                ...state,
                animals: action.payload
            }    
        
        default : 
            return state
    }

}