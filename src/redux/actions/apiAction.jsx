import axios from "axios"

export const GET_POST = "[API] getPost"

//Actions
const getPost = (data) => ({
    type: GET_POST,
    payload: data
});

//Esto es lo que se exporta
export const callToApiRedux = () => {

    return async (dispatch) => {

        const res = await axios.get('https://my-json-server.typicode.com/DeproWeb/adoptions/db')
        const data = res.data.adoptions
        dispatch(getPost(data))
        console.log(data);

    }
};
