
//Esto son simplementes strings.
export const REGISTER = "[AUTH] register";
export const LOGIN = "[AUTH] login";
export const LOGOUT = "[AUTH] logout";

//Estas son las acciones
const actionRegister = (data) => ({

    type: REGISTER,

    payload: data

})

const actionLogin = (data) => ({
    
    type: LOGIN,

    payload: data
})

const actionLogout = () => ({

    type: LOGOUT,
    
    payload: {}

})

//Esto es lo que se exporta, las funciones: 
//(Tú puedes hacer lo que quieras aquí dentro, siempre y cuando luego uses los dispatch que llaman al reducer)

export const RegisterUser = (data) => {
    return async (dispatch) =>{ //Esto se hace siempre para entrar con el dispatch

        const request = await fetch("http://localhost:4000/auth/register", {
            method: "POST",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                "Access-Control-Allow-Origin": "*",
            },
            credentials: "include",
            body: JSON.stringify(data),
            });
            
        const result = await request.json();

        dispatch(actionRegister(result))

    }
}


export const LoginUser = (data) => {

    return async (dispatch) => {
    
        const request = await fetch("http://localhost:4000/auth/login", {
            method: "POST",
            headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
            },
            credentials: "include",
            body: JSON.stringify(data),
        });
    
        const result = await request.json();

        dispatch(actionLogin(result))
}
}

export const Logout = () => {
    return (dispatch) =>{
    
    dispatch(actionLogout());

    }
}