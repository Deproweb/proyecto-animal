import './App.scss';

//Hooks

//Router-Dom
import {
  BrowserRouter,
  Routes,
  Route
} from "react-router-dom";

//Pages
import { HomePage } from './pages/HomePage/HomePage';
import { ShopPage } from './pages/ShopPage/ShopPage';
import AdoptionPage from './pages/AdoptionsPage/AdoptionPage';
import { ContactPage } from './pages/ContactPage/ContactPage';

//Components
import Header  from './components/Header/Header';
import { Footer } from './components/Footer/Footer';
import { AuthPage } from './pages/AuthPage/AuthPage';
import { NotAdoption } from './components/NotAdoption/NotAdoption';

//Funciones
import { connect } from 'react-redux';
import AnimalPage from './pages/AnimalPage/AnimalPage';
import { Cart } from './components/Cart/Cart';

function App(props) {

  console.log(props.user)

  return (
    <div className="app">
      <BrowserRouter>
      <Header user={props.user} />
          <Routes>
          <Route path="/">
            <Route index element={<HomePage/>} />
            <Route path="shop" element={<ShopPage />}/>
            <Route path="login" element={<AuthPage />}/>
            <Route path="adoptions">
            {props.user && <Route index element={<AdoptionPage />} />}
            {!props.user && <Route index element={<NotAdoption />} />}
            <Route path=":animal" element={<AnimalPage  />}/>
            </Route>
            <Route path="contact" element={<ContactPage/>}/>
            <Route path="contact" element={<Cart/>}/>
          </Route>
        </Routes>
        <Footer/>
      </BrowserRouter>
    </div>
  );
}

const mapStateToProps = (state) => ({
  user: state.auth.user,
})

export default connect(mapStateToProps)(App);
