import { useEffect, useState} from 'react';
import { connect } from 'react-redux';
import { callToApi } from '../../api/callToApi';
import './GalleryShop.scss'
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button'

export default function GalleryShop (props) {

    const [products, setProducts] = useState([]);

    const listApiFn = async () => {
    const listProducts = await callToApi()
    setProducts(listProducts.data.products);
    }

    useEffect(() => {
        listApiFn()
    }, []);

    return (<div className='c-galleryShop'>

        <h4 className='c-galleryShop__title'>Artículos</h4>
        <div className='c-galleryShop__products'>
        {products.map((product => 
            <Card className='c-galleryShop__products__product' key={product.name} style={{ width: '18rem', height: '350px', border: '2px solid white' }}>
                <Card.Img variant="top" src={product.img} />
                <Card.Body>
                <Card.Title>{product.name}</Card.Title>
                <Card.Text>
                    Price: {product.price} €
                </Card.Text>
                <Button variant="warning">Comprar</Button>
                </Card.Body>
            </Card>
        ))}
        </div>
    </div>);
};


