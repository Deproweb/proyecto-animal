import './NotAdoptions.scss'

export const NotAdoption = () => {
    return (<div className="c-notAdoption">
        <div className="c-notAdoption__info animate__animated animate__pulse">
            <h4>Para acceder al apartado de adopciones debes estar registrado</h4>
        </div>
        <img className="c-notAdoption__img" src="https://cdn.pixabay.com/photo/2017/04/11/15/55/animals-2222007_1280.jpg" alt="" />
    </div>);
};
