import './FormAuth.scss'
import { useForm } from "react-hook-form";
import { RegisterUser } from '../../redux/actions/authAction';
import { connect } from 'react-redux';
import { useNavigate } from 'react-router';

const FormAuth = (props) => {

    const { register, handleSubmit, formState: { errors } } = useForm();

    const navigate = useNavigate()

    const onSubmit = (data, ev) => {
        props.dispatch(RegisterUser(data))
        console.log(data);
        ev.target.reset()
        navigate("/")
    };

    const onSubmitLog = (data, ev) => {
        props.dispatch(RegisterUser(data))
        console.log(data);
        ev.target.reset()
        navigate("/")
    }

    return (

        <>

        <div  className="formIMG animate__animated animate__flipInY">
            <img className="formIMG__img animate__animated animate__flipInY " src="https://cdn.pixabay.com/photo/2018/10/01/09/21/pets-3715733_1280.jpg" alt="LoginImg" />
        </div>

        <div className="formAuth">

        <h3 className="formAuth__title">Regístrate</h3>

        <form className="formAuth__form" onSubmit={handleSubmit(onSubmit)}>

        <input className="formAuth__form__input" placeholder="Username" type="text" {...register("username", { required: true })} />

        <input className="formAuth__form__input" type="text" placeholder="Email" {...register("email", {required: true})} />
        {errors.exampleRequired && <span>This field is required</span>}

        <input className="formAuth__form__input" placeholder="Password" type="password" {...register("password", { required: true })} />

        <input className="formAuth__form__button" type="submit" />

        </form>

        </div>
        </>
    );
};

export default connect()(FormAuth);