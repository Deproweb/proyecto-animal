import Nav from 'react-bootstrap/Nav'
import { connect } from 'react-redux';
import { Link} from 'react-router-dom';
import { Logout } from '../../redux/actions/authAction';

import './Header.scss'

const Header = ({user}, props) => {

    return (<div className='c-header'>
                <div className="c-header__logo">
                    <h3>Upgrade Animals</h3>
                </div>
                <Nav className="justify-content-center c-header__nav" activeKey="/home">
                    <Nav.Item>
                        <Link to="/" className="c-header__link">Home</Link> 
                    </Nav.Item>
                    <Nav.Item>
                        <Link to="/shop" className="c-header__link">Shop</Link> 
                    </Nav.Item>
                    <Nav.Item>
                        <Link to="/adoptions" className="c-header__link">Adoptions</Link> 
                    </Nav.Item>
                    <Nav.Item>
                        <Link to="/contact" className="c-header__link">Contact</Link> 
                    </Nav.Item>
                    { !user && <Nav.Item>
                        <Link to="/login" className="c-header__link">Register/Login</Link> 
                    </Nav.Item>}
                    { user && <Nav.Item>
                        <Link to="#" onClick={()=>{props.dispatch(Logout())}}  className="c-header__link">Logout</Link> 
                    </Nav.Item>}
                </Nav>
    </div>);
};

export default connect()(Header)