import { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { callToApiRedux } from '../../redux/actions/apiAction';
import Card from 'react-bootstrap/Card'
import Button from 'react-bootstrap/Button'
import { useNavigate } from 'react-router';

import './AdoptionPage.scss'

const AdoptionPage = ({animals, dispatch}) => {

    const navigate = useNavigate()

    const sendInfo = (animal) => {
        navigate(`/adoptions/${animal.name}`)
    }

    useEffect(() => {
        dispatch(callToApiRedux())
    }, []);

    return <div className="p-adoptionPage">
        <img className="p-adoptionPage__img " src="https://cdn.pixabay.com/photo/2017/08/07/18/57/dog-2606759_1280.jpg" alt="" />
        <div className="p-adoptionPage__list">
        {animals.map(animal => 
            <Card className="p-adoptionPage__list__card" key={animal.name} bg="success" style={{ width: '18rem', height: "450px", margin: '15px' }}>
                <Card.Img style={{ height: "230px"}} variant="top" src={animal.img} />
                <Card.Body>
                    <Card.Title>{animal.name}</Card.Title>
                    <Card.Text>
                    {animal.description}
                    </Card.Text>
                    <Button onClick={()=>{sendInfo(animal)}}   variant="warning">I want adopt!</Button>
                </Card.Body>
            </Card>
        )}
        </div>
    </div>;
};

const mapPropsToState = (state) => ({
    animals: state.api.animals
})

export default connect(mapPropsToState)(AdoptionPage)
