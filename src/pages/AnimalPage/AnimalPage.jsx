import { useEffect, useState } from "react";
import { connect } from "react-redux";
import { useParams } from "react-router";

import './AnimalPage.scss' 

const AnimalPage = ({animals}) => {

    const {animal} = useParams()

    const [animalFound, setAnimalFound] = useState({})

    useEffect(() => {
        animals.forEach(element => {
            if(element.name === animal) {   
                setAnimalFound(element)
            }
        });
    }, []); 


    return <>
        <h1 className="animal__title">{animalFound.name}</h1>
        <p className="animal__description">{animalFound.description}</p>
        <img src={animalFound.img} alt="imagen_animal" />
    </>;
};

const mapPropsToState = (state) => ({
    animals: state.api.animals
})

export default connect(mapPropsToState)(AnimalPage)