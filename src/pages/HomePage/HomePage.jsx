import { AccordionC } from '../../components/Accordion/Accordion';
import './HomePage.scss'

export const HomePage = () => {
    return (<div className='c-homePage' >

        <img src="https://cdn.pixabay.com/photo/2016/10/31/14/55/rottweiler-1785760_1280.jpg" alt="img-rotwailers" />
    
        <div className='c-homePage__info'>
            <h3>Bienvenido/a a Ugrade Animals</h3>
            <h5>En nuestra web podrás encontrar productos de todo tipo para tus mascotas</h5>
            <h5>También podrás encontrar algunos animales en adopción a través de las protectoras con las que trabajamos</h5>
        </div>

        <div className='c-homePage__accordion'>
            <AccordionC/>
        </div>

    </div>);
};
