import './ContactPage.scss'
import { useForm } from "react-hook-form";

export const ContactPage = () => {
    const { register, handleSubmit } = useForm();
    const onSubmit = data => console.log(data);

    return (
    <form className="c-form" onSubmit={handleSubmit(onSubmit)}>
        <input placeholder="Name" className="c-form__input" {...register("Name")} />
        <input placeholder="Email" className="c-form__input" {...register("Email")} />
        <input placeholder="Message" className="c-form__input__text" type="textarea" {...register("Consulta")} />
        <input className="c-form__button" type="submit" />
    </form>
    );
};

