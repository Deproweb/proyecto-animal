
import './ShopPage.scss'
import GalleryShop from './../../components/GalleryShop/GalleryShop'

export const ShopPage = () => {
    return (<div className="c-shopPage">

        <img className="animate__animated animate__backInRight" src="https://cdn.pixabay.com/photo/2017/02/20/18/03/cat-2083492_1280.jpg" alt="cat-img" />

        <GalleryShop></GalleryShop>

    </div>);
};
